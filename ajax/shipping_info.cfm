<cfsetting showdebugoutput="no">

<cfinclude template="../serverSettings.cfm">

<cfscript>	
	if (#FindNoCase('application/json', cgi.content_type)# > 0)
	{
    	myData = deserializeJSON(ToString(getHTTPRequestData().content));
		key = myData.key;
		who = myData.who;
	}
	else
	{
		key = URL.key;
		who = URL.who;
	}
	switch(who){
		case "supplier_info":
			dao = 'getshipping';
			break;
		case "shipper_info":
			dao = "getshipping";
			break;
	}
	
	object = CreateObject("component", "#cfcs#.productReq"); 
	returnResult = Evaluate("object.#dao#(key = ""#key#"")"); 

	serializer = new lib.JsonSerializer()
		.asString( "street" )
		.asString( "city" )
		.asString( "state_code" )
		.asString( "zip" )
	;
	writeOUtput(serializer.serialize(returnResult));
</cfscript>