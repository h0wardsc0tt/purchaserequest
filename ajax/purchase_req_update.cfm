
<cfsetting showdebugoutput="no">
<cfparam name="URL.lookup" default="">
<cfinclude template="../serverSettings.cfm">

<cfscript>	

	
	if (#FindNoCase('application/json', cgi.content_type)# > 0)
	{
    	json = deserializeJSON(ToString(getHTTPRequestData().content));
		type = json.type;
		purchase_req  = json.json;
	}
	else
	{
		//phrase = URL.lookup;
		//search_type = URL.seachType;
		//search_for  = URL.searhFor;
	}

 	object = CreateObject("component", "_cfcs.productReq");
	order = purchase_req.order;

	returnResult = object.updatePurchaseReq(
		id = #purchase_req.key#,
		type = "#type#",
		purchaseOrder = "#order.po#",
		buyer = "#order.buyer#",
		shipVia = "#order.shipvia#",
		supplier = "#order.supplier_edit#",
		shipper = "#order.shipping_edit#",
		requestor = "#order.requestor#",
		requestedDate = "#order.requestor_date#",
		accountNumber = "#order.account_number#",
		department = "#order.dept#",	
		comments = "#order.comments#"
	); 

	returnObj = DeserializeJSON(returnResult);
	pr_key = #purchase_req.key#;
	if(pr_key == '')
		pr_key = #returnObj.identity#;

	for(i = 1; i LTE ArrayLen(purchase_req.line_items); i++){
		liArray = purchase_req.line_items[i];
		li = liArray.line_item;
		returnResultLineitem = object.updatePurchaseReqLineitem(
			id = #liArray.key#,
			key = #pr_key#,
			type = "#type#",
			lineItem = "#li.lineItem#",
			quantity = #li.quantity#,
			umo = "#li.umo#",
			estcost = "#ReplaceNoCase(ReplaceNoCase(li.estcost,'$',''),',','','ALL')#",
			needdate = "#li.needdate#",
			manufacturedesc = "#li.manufacturedesc#",
			purchasequantity = #li.purchasequantity#,
			price = "#ReplaceNoCase(ReplaceNoCase(li.price,'$',''),',','','ALL')#",
			dockDate = "#li.dockDate#",
			lineItemId = "#liArray['id']#"
		); 			 
		returnLineItemObj = DeserializeJSON(returnResultLineitem);
		arrayAppend(returnObj.lineItems, returnLineItemObj, true);
	}

	//if(type == 'update' && purchase_req.status == 'new')
	//	type = 'new';
	//response = '{"status":"OK", "type":"' & type & '", "error":""}';
	//response = '{"status":"OK", "error":"'  & purchase_req.status & '"}';
	//response = '{"status":"OK", "error":"'  & purchase_req.order[1].value & '"}';

	//writeOutput(purchase_req.status);
	//writeOutput(serializer.serialize(purchase_req));
	serializer = new lib.JsonSerializer();
	writeOutput(serializer.serialize(returnObj));
</cfscript>



    
  


