<cfparam name="Page_MetaTitle" default="">
<cfparam name="Page_MetaKeywords" default="">
<cfparam name="Page_MetaDescription" default="">

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><cfoutput>#Page_MetaTitle#</cfoutput></title>
        <meta name="Title" content="<cfoutput>#Page_MetaTitle#</cfoutput>">
        <meta name="Description" content="<cfoutput>#Page_MetaDescription#</cfoutput>">
        <meta name="Keywords" content="<cfoutput>#Page_MetaKeywords#</cfoutput>">
        <meta name="format-detection" content="telephone=no">
        <link rel="icon" href="favicon.ico?v=3">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="./css/main.css">
        <link rel="stylesheet" href="./css/Manuals.css?v=8">
        <link rel="stylesheet" href="./css/animate.css">
        <link rel="stylesheet" href="./css/materialize.css">
    </head>
    <body>

		<cfif NOT User_IsAtLogin>
			<cfif User_IsLoggedIn>
                <cfinclude template="./_tmp_HTML_Navigation_LoggedIn.cfm">
            <cfelse>
                <cfinclude template="./_tmp_HTML_Navigation_LoggedOut.cfm">
            </cfif>
        </cfif>
        