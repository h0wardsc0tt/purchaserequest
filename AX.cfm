
<!--- READ --->

<cfsavecontent variable="soapBody">
<cfoutput>
<?xml version="1.0" encoding="utf-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://schemas.microsoft.com/dynamics/2010/01/datacontracts" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:ser="http://schemas.microsoft.com/dynamics/2008/01/services" xmlns:ent="http://schemas.microsoft.com/dynamics/2006/02/documents/EntityKeyList" xmlns:ent1="http://schemas.microsoft.com/dynamics/2006/02/documents/EntityKey">
    <soapenv:Header>
      <dat:CallContext>
         <dat:Company>HSC</dat:Company>
         <dat:LogonAsUser>HME\chrisg</dat:LogonAsUser>
      </dat:CallContext>
   </soapenv:Header>
    <soapenv:Body>
      <ser:SalesOrderServiceReadRequest>
         <!--Optional:-->
         <ent:EntityKeyList>
            <!--Zero or more repetitions:-->
            <ent1:EntityKey>
               <ent1:KeyData>
                  <!--1 or more repetitions:-->
                  <ent1:KeyField>
                     <ent1:Field>SalesId</ent1:Field>
                     <ent1:Value>1451828</ent1:Value>
                  </ent1:KeyField>
               </ent1:KeyData>
            </ent1:EntityKey>
         </ent:EntityKeyList>
      </ser:SalesOrderServiceReadRequest>
    </soapenv:Body>
</soapenv:Envelope>
</cfoutput>
</cfsavecontent>

<cfhttp url="http://powaxdev.hme.com:83/MicrosoftDynamicsAXAif60/WebSalesOrder/xppservice.svc?wsdl" method="POST" result="soapResponse" domain="HME" username="AX2012-BCProxy" password="HME92064!" authType="NTLM" redirect="true">
    <cfhttpparam type="header" name="accept-encoding" value="no-compression" />
    <cfhttpparam type="header" name="SOAPAction" value="http://schemas.microsoft.com/dynamics/2008/01/services/SalesOrderService/read" />
    <cfhttpparam type="xml" value="#trim(soapBody)#" />
</cfhttp>

<!---<cfdump var="#soapResponse.filecontent#">--->

<cfxml variable="test">
	<cfoutput>#soapResponse.filecontent#</cfoutput>
</cfxml>

<cfdump var="#test#">
