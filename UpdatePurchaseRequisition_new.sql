USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[UpdatePurchaseRequisition]    Script Date: 9/11/2016 12:26:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[UpdatePurchaseRequisition](
     @id INT,
     @purchaseOrder NVARCHAR(25) = NULL,
     @buyer NVARCHAR(25) = NULL,
     @shipVia NVARCHAR(25) = NULL,
     @supplier NVARCHAR(1000) = NULL,
     @shipper NVARCHAR(1000) = NULL,
     @requestor NVARCHAR(25) = NULL,
     @requestedDate NVARCHAR(25) = NULL,
     @accountNumber NVARCHAR(25) = NULL,
     @department NVARCHAR(25) = NULL,
     @comments NVARCHAR(4000) = NULL,
     @type NVARCHAR(25) = NULL,
	 @updateType NVARCHAR(10) OUTPUT,
	 @identity INT OUTPUT

) AS

SET  @identity = 0

if  @type IS NOT NULL and @type = 'delete'
	BEGIN
		SET @updateType = 'delete'
		DELETE from dbo.PurchaseRequisition WHERE id =  @id
	END
ELSE
	BEGIN
		if EXISTS (SELECT purchaseOrder from dbo.PurchaseRequisition WHERE id =  @id)
			BEGIN
				SET @updateType = 'update'
				UPDATE dbo.PurchaseRequisition
				SET		purchaseOrder = @purchaseOrder,
						buyer = ISNULL(@buyer, buyer), 
						shipVia = ISNULL(@shipVia, shipVia),
						supplier = ISNULL(@supplier, supplier),
						shipper = ISNULL(@shipper, shipper),
						requestor = ISNULL(@requestor, requestor),
						requestedDate = ISNULL(@requestedDate, requestedDate),
						accountNumber = ISNULL(@accountNumber, accountNumber),
						department = ISNULL(@department, department),
						comments = ISNULL(@comments, comments),
						modifiedDate = GETDATE()
				WHERE id =  @id
			END
		ELSE
			BEGIN
				SET @updateType = 'insert'
				INSERT INTO dbo.PurchaseRequisition
					(purchaseOrder, buyer, shipVia, supplier, shipper, requestor, requestedDate, accountNumber, department, comments)
				VALUES
					(@purchaseOrder, @buyer, @shipVia ,@supplier ,@shipper, @requestor, @requestedDate, @accountNumber, @department, @comments)
				SET  @identity = @@IDENTITY
			END
	END

