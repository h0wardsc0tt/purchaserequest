USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[SelectPurchaseRequisition]    Script Date: 9/13/2016 8:22:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SelectPurchaseRequisition](
     @id INT = NULL,
     @purchaseOrder NVARCHAR(25) = NULL,
     @buyer NVARCHAR(25) = NULL,
     @shipVia NVARCHAR(25) = NULL,
     @supplier NVARCHAR(1000) = NULL,
     @shipper NVARCHAR(1000) = NULL,
     @requestor NVARCHAR(25) = NULL,
     @requestedDate NVARCHAR(25) = NULL,
     @accountNumber NVARCHAR(25) = NULL,
     @department NVARCHAR(25) = NULL,
     @comments NVARCHAR(4000) = NULL

) AS
DECLARE
	@select NVARCHAR(4000),
	@from	NVARCHAR(4000),
	@where	NVARCHAR(4000),
	@sql	NVARCHAR(4000)

IF @id IS NOT NULL 
	BEGIN
		SELECT 
			pr.id AS [key], 
			pr.Supplier AS supplier_edit, 
			pr.Shipper AS shipping_edit, 
			pr.Requestor AS requestor,
			pr.RequestedDate AS requestor_date,
			pr.AccountNumber as account_number,
			pr.Department AS dept,
			pr.PurchaseOrder AS po,
			pr.Buyer AS buyer,
			pr.ShipVia AS shipvia,
			pr.comments
		FROM PurchaseRequisition pr 
		WHERE pr.id = @id
	END
ELSE 
BEGIN
	SET @select = 'SELECT pr.id, pr.PurchaseOrder, pr.Requestor, pr.RequestedDate, li.[key], li.ManufacturePinDescription'
	SET @from =  ' FROM PurchaseRequisition pr INNER JOIN PurchaseRequisitionLineItems li ON  li.[Key] =  pr.id'
	SET @where = ' WHERE 1=1'

	IF @supplier IS NOT NULL
		BEGIN
			SET @where = @where + ' AND Supplier LIKE ' + '''%' + @supplier + '%'''
		END
	IF @purchaseOrder IS NOT NULL
		BEGIN
			SET @where = @where + ' AND PurchaseOrder LIKE ' + '''%' + @purchaseOrder + '%'''
		END

	SET @sql = @select + @from + @where

	print @sql
	EXEC sp_executesql @sql
END