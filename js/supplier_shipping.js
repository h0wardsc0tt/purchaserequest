function supplier_shippingInfo(jsonObj){
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: "ajax/supplier_shipping.cfm",
		data: '{"key":"' + jsonObj.item.key + '","who":"' + jsonObj.item.who + '"}',
		dataType: "json",
		success: function (data) {	
			var json;										
			if (data !== null && data.length > 0 ) {
				json = data[0];	
				json.status = 'OK';
				json.who = jsonObj.item.who;
				json.key = jsonObj.item.key;
			}else{
				json = JSON.parse('{"status":"error","errorThrown":"No records returned"}');
				json.who = jsonObj.item.who;
				json.key = jsonObj.item.key;
			}								
			shipping_supplier_update(json);								
		},
		
		error: function (jqXHR, textStatus, errorThrown)
		{
			json = JSON.parse('{"status":"' + textStatus + '","errorThrown":"' + errorThrown + '"}');
			json.who = jsonObj.item.who;
			json.key = jsonObj.item.key;
			shipping_supplier_update(json);								
		}
	});	
}

function shipping_supplier_update(json_response){
	console.log(json_response);
	var update = '',  msg_obj, index;
	switch(json_response.who){
	case "supplier_info":
		index = json.order.map(function (id) { return id['id']; }).indexOf('supplier_edit');
		msg_obj = $('#supplier_error_message');
		supplierspinner.stop();
		break;
	case "ship_to_info":
		index = json.order.map(function (id) { return id['id']; }).indexOf('shipping_edit');
		msg_obj = $('#shipping_error_message');
		shipperspinner.stop();
		break;
	}
	console.log(json_response.who);
	var obj = $('#'+json_response.who);
	console.log(obj);
	if(json_response.status == 'OK'){
		update += json_response.street +'\n';
		update += json_response.city + ', ' + json_response.state_code + ' ' + json_response.zip;
	
		obj.children('.supplier-shipping-content').children('.content-edit').val(update);
		json.order[index].value = update;
		json.order[index].status = 'modified';
		json.order_change = 'modified';
		$('#save_purchase_req').prop('disabled', false);
		console.log(json);

		console.log(obj.children('.supplier-shipping-content').children('.content-edit').val());
		obj.children('a').click();
	}else{
		var err_mag_obj = msg_obj.children('.system-message');
		err_mag_obj.html('Could not complete the request:<br>' + json.errorThrown);
		msg_obj.height(err_mag_obj.prop('scrollHeight'));
	}

}