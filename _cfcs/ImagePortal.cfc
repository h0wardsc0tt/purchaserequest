<cfcomponent name="ImagePortal" hint="Image Portal related functions">
	<!----<cffunction name="init" access="public" output="false" returntype="any">
    	<cfargument type="String" name="dns" required="true" default="#APPLICATION.Datasource#">
        
		<cfscript>
			Variables.dns = Arguments.dns;
			return this;
		</cfscript>
	</cffunction>    
--->       
	<!--- Get binary Image functions --->
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
    <cffunction name="getImageById" output="false" returntype="query" access="public" description="Return binary Image">
    	<cfargument type="string" name="ImageID" required="yes">
        <cftry>
            <cfquery name="local.qry_GetImage" datasource="#APPLICATION.Datasource#">
           		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
            	SET NOCOUNT ON
            
                SELECT	i.Image, i.ImageType, i.ImageName, i.ImagePrefix, i.ImageAnchorText, 
                i.ImageDescription, i.ImageSort, i.ProductOrder, i.Product, i.Guid, c.categoryGuid,
                (select count(*) from ImagePortal.Images where product = i.product) as imageCount
                FROM	ImagePortal.Images i
                INNER   JOIN ImagePortal.Categories c on c.categoryId = i.categoryId
                WHERE	i.Guid = <cfqueryparam cfsqltype="CF_SQL_CHAR" value="#Arguments.ImageID#">
            </cfquery>
        
            <cfcatch type="database"></cfcatch>
        </cftry>
    	
        <cfreturn local.qry_GetImage />
    </cffunction>
 
<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->   
   <cffunction name="getCategoriesAndProductImages" output="false" returntype="query" access="public" description="Return all Categories and Images">
		<cftry>
			<cfstoredproc procedure="[dbo].[ImageProtal_PortalSelect]" datasource="#APPLICATION.Datasource#" >                            
            	<cfprocresult name="qry_ImageProtal_PortalSelect">     
			</cfstoredproc>	
            <cfcatch type="Database"></cfcatch>
		</cftry>
		<cfreturn qry_ImageProtal_PortalSelect />
    </cffunction>
    
<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
	<cffunction name="SearchCategoriesAndProductImages" output="false" returntype="query" access="public" description="Return all Categories and Images">
    	<cfargument type="string" name="SearchFor" required="yes">
		<cftry>
			<cfstoredproc procedure="[dbo].[ImageProtal_PortalFilter]" datasource="#APPLICATION.Datasource#" >                            
                <cfprocparam
                  cfsqltype="CF_SQL_VARCHAR"
                  value="#Arguments.SearchFor#"> 
            	<cfprocresult name="qry_ImageProtal_PortalFilter">     
			</cfstoredproc>	
            <cfcatch type="Database"></cfcatch>
		</cftry>
		<cfreturn qry_ImageProtal_PortalFilter />
    </cffunction>
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->  
	<cffunction name="getLinkType" output="false" returntype="string" access="public" description="Return Link Type">
        <cfargument type="string" name="prefix" required="yes">
    		<cfset linkType = "">
            <cfswitch expression="#Arguments.prefix#">  
                <cfcase value="jpg">  
                    <cfset linkType = "Low Resolution JPG">   
                </cfcase>   
                <cfcase value="tif">  
                    <cfset linkType = "High Resolution TIF">>   
                </cfcase>   
                <cfcase value="tiff">  
                    <cfset linkType = "High Resolution TIF">>   
                </cfcase>   
                <cfcase value="png">  
                    <cfset linkType = "Low Resolution PNG">>   
                </cfcase>   
                <cfcase value="eps">  
                    <cfset linkType = "High Resolution EPS">>   
                </cfcase>   
                <cfcase value="doc">  
                    <cfset linkType = "Editable Microsoft Word">>   
                </cfcase>   
                <cfcase value="docx">  
                    <cfset linkType = "Editable Microsoft Word">>   
                </cfcase>   
                <cfcase value="pdf">  
                    <cfset linkType = "PDF">>   
                </cfcase>   
            </cfswitch>     	
        <cfreturn linkType />
	</cffunction>
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->  
	<cffunction name="getImageType" output="false" returntype="string" access="public" description="Return Image Type">
        <cfargument type="string" name="linkType" required="yes">
        <cfswitch expression="#Arguments.linkType#">  
            <cfcase value="Low Resolution JPG">  
                <cfset imageType = "3">   
            </cfcase>   
            <cfcase value="High Resolution TIF">  
                <cfset imageType = "4">>   
            </cfcase>   
            <cfcase value="Low Resolution PNG">  
                <cfset imageType = "3">>   
            </cfcase>   
            <cfcase value="High Resolution EPS">  
                <cfset imageType = "4">>   
            </cfcase>   
            <cfcase value="Editable Microsoft Word">  
                <cfset imageType = "4">>   
            </cfcase>   
            <cfcase value="PDF">  
                <cfset imageType = "4">>   
            </cfcase>   
        </cfswitch>    	
        <cfreturn imageType />
    </cffunction>
    
 <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->  
	<cffunction name="updateImageAndDescrption" returntype="string" output="false"  access="public" description="Update Image and Description">
        <cfargument type="binary" name="image"  dbvarname="@image" required="yes">
        <cfargument type="string" name="imageGuid" dbvarname="@guid" required="yes">
        <cfargument type="string" name="imageDescription" dbvarname="@imageDescription" required="no">
        <cfset results = "OK">  
        <cftry>
            <cfstoredproc procedure="dbo.ImageProtal_UpdateImageAndImageDescriptions" datasource="#APPLICATION.Datasource#" >         
                <cfprocparam
                  cfsqltype="cf_sql_blob"
                  value="#Arguments.image#"> 
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.imageGuid#">
                 <cfif isDefined("Arguments.imageDescription")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        value="#Arguments.imageDescription#">
				</cfif>                    
				<cfprocresult name="qryupdateImageAndDescrption">     
			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  
            </cfcatch>
        </cftry>  	
		<cfreturn results />
	</cffunction>        

 <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->  
	<cffunction name="updateImageDescription" returntype="string" output="false"  access="public" description="Update Image Description">
        <cfargument type="string" name="imageGuid" dbvarname="@guid" required="yes">
        <cfargument type="string" name="imageDescription" dbvarname="@imageDescription" required="no">
        
        <cfset results = "OK">  
        <cftry>
            <cfstoredproc procedure="dbo.ImageProtal_UpdateImageDescription" datasource="#APPLICATION.Datasource#" >         
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.imageGuid#">
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.imageDescription#">                  
				<cfprocresult name="qry_UpdateImageDescription">     
			</cfstoredproc>	
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#"> 
            </cfcatch>
        </cftry>            
		<cfreturn results />
	</cffunction>            
        
 <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->  
	<cffunction name="updateImage" returntype="string" output="false"  access="public" description="Update Image">
        <cfargument type="string" name="imageGuid" dbvarname="@guid" required="yes">
        <cfargument type="string" name="imageDescription" dbvarname="@imageDescription" required="no">
        <cfset results = "OK">  
        <cftry>
            <cfstoredproc procedure="dbo.ImageProtal_UpdateImage" datasource="#APPLICATION.Datasource#" >         
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.imageDescription#">                   
            	<cfprocresult name="qry_UpdateImage">     
			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  
            </cfcatch>
		</cftry>	
		<cfreturn results />
	</cffunction> 
    
 <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->  
	<cffunction name="updateProductLink" returntype="string" output="false"  access="public" description="Update Product Link">
        <cfargument type="binary" name="image"  dbvarname="@image" required="yes">
        <cfargument type="string" name="imageGuid" dbvarname="@guid" required="yes">
        <cfargument type="string" name="imageType" dbvarname="@imageType" required="yes">
        <cfargument type="string" name="imageAnchorText" dbvarname="@mageAnchorText" required="yes">
        <cfargument type="string" name="imageName" dbvarname="@imageName" required="yes">
        <cfargument type="string" name="imagePrefix" dbvarname="@imagePrefix" required="yes">
        <cfset results = "OK">  
        <cftry>
            <cfstoredproc procedure="ImageProtal_UpdateProductLink" datasource="#APPLICATION.Datasource#" >         
              <cfprocparam
                  cfsqltype="cf_sql_blob"
                  value="#Arguments.image#"> 
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.imageGuid#">
               <cfprocparam
                    cfsqltype="cf_sql_integer"
                    value="#Arguments.imageType#">
               <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.imageAnchorText#">
               <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.imageName#">
               <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.imagePrefix#">
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  
            </cfcatch>
		</cftry>	
		<cfreturn results />
	</cffunction>   
    
 <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->  
	<cffunction name="addNewProductLink" returntype="string" output="false"  access="public" description="Add New Product Link">
        <cfargument type="binary" name="image"  dbvarname="@image" required="yes">
        <cfargument type="string" name="guid" dbvarname="@guid" required="yes">
        <cfargument type="string" name="imageName" dbvarname="@imageName" required="yes">
        <cfargument type="string" name="imagePrefix" dbvarname="@imagePrefix" required="yes">
        <cfset results = "OK">  
        <cftry>
            <cfstoredproc procedure="ImageProtal_AddNewProductLink" datasource="#APPLICATION.Datasource#" >         
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.guid#">
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.imageName#">
              	<cfprocparam
                  cfsqltype="cf_sql_blob"
                  value="#Arguments.image#"> 
               	<cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.imagePrefix#">
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  
            </cfcatch>
		</cftry>	
		<cfreturn results />
	</cffunction>       
                  
 <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->  
	<cffunction name="setIsActive" returntype="string" output="false"  access="public" description="Set the image isActive to inactive">
        <cfargument type="string" name="imageGuid" dbvarname="@guid" required="yes">
        <cfset results = "OK">  
        <cftry>
            <cfstoredproc procedure="[dbo].[ImageProtal_SetImageInactive]" datasource="#APPLICATION.Datasource#" >         
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.imageGuid#">                   
            	<cfprocresult name="qry_SetInActive">     
			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  
            </cfcatch>
		</cftry>	
		<cfreturn results />
	</cffunction> 
    
 <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->  
	<cffunction name="setProductIsActive" returntype="string" output="false"  access="public" description="Set the product to inactive">
        <cfargument type="string" name="imageGuid" dbvarname="@guid" required="yes">
        <cfset results = "OK">  
        <cftry>
            <cfstoredproc procedure="[dbo].[ImageProtal_setProductIsActive]" datasource="#APPLICATION.Datasource#" >         
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.imageGuid#">                   
            	<cfprocresult name="qry_setProductIsActive">     
			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  
            </cfcatch>
		</cftry>	
		<cfreturn results />
	</cffunction> 
    
    
 <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->  
	<cffunction name="setCategoryIsActive" returntype="string" output="false"  access="public" description="Set the category to inactive">
        <cfargument type="string" name="categoryGuid" dbvarname="categoryGuid" required="yes">
        <cfset results = "OK">  
        <cftry>
            <cfstoredproc procedure="[dbo].[ImageProtal_SetCategoryIsActive]" datasource="#APPLICATION.Datasource#" >         
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.categoryGuid#">                   
            	<cfprocresult name="qry_SetCategoryIsActive">     
			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  
            </cfcatch>
		</cftry>	
		<cfreturn results />
	</cffunction> 
  
 <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->  
	<cffunction name="AddNewProduct" returntype="string" output="false"  access="public" description="Add new Category Product">
        <cfargument type="binary" name="image"  dbvarname="@image" required="yes">
        <cfargument type="string" name="categoryGuid" dbvarname="@categoryGuid" required="yes">
        <cfargument type="string" name="imageAnchorText" dbvarname="@mageAnchorText" required="yes">
        <cfargument type="string" name="imageName" dbvarname="@imageName" required="yes">
        <cfargument type="string" name="imagePrefix" dbvarname="@imagePrefix" required="yes">
        <cfargument type="string" name="imageDescription" dbvarname="@imageDescription" required="yes">
        <cfset results = "OK">  
        <cftry>
            <cfstoredproc procedure="ImageProtal_AddNewProduct" datasource="#APPLICATION.Datasource#" >         
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.categoryGuid#">
              <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.imageAnchorText#">
               <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.imageName#">
               <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.imageDescription#">
              <cfprocparam
                  cfsqltype="cf_sql_blob"
                  value="#Arguments.image#"> 
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.imagePrefix#">
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  
            </cfcatch>
		</cftry>	
		<cfreturn results />
	</cffunction>                         
                  
       
 <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->  
	<cffunction name="UpdateCategoryImage" returntype="string" output="false"  access="public" description="UUpdate Category">
        <cfargument type="binary" name="image"  dbvarname="@image" required="yes">
        <cfargument type="string" name="categoryGuid" dbvarname="@categoryGuid" required="yes">
        <cfargument type="string" name="categoryDescription" dbvarname="@categoryDescription" required="yes">
        <cfargument type="string" name="categoryName" dbvarname="@categoryName" required="yes">
        <cfargument type="string" name="imageName" dbvarname="@imageName" required="yes">
        <cfargument type="string" name="imagePrefix" dbvarname="@imagePrefix" required="yes">
        <cfset results = "OK">  
        <cftry>
            <cfstoredproc procedure="dbo.ImageProtal_UpdateCategoryImage" datasource="#APPLICATION.Datasource#" >         
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.categoryGuid#">                   
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.categoryDescription#">
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.categoryName#">
                <cfprocparam
                  cfsqltype="cf_sql_blob"
                  value="#Arguments.image#"> 
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.imageName#">
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.imagePrefix#">
				<cfprocresult name="qry_UpdateCategoryImage">     
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  
            </cfcatch>
		</cftry>	
		<cfreturn results />
	</cffunction> 
  
        
 <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->  
	<cffunction name="NewCategory" returntype="string" output="false"  access="public" description="UUpdate Category">
        <cfargument type="binary" name="image"  dbvarname="@image" required="yes">
        <cfargument type="string" name="categoryDescription" dbvarname="@categoryDescription" required="yes">
        <cfargument type="string" name="categoryName" dbvarname="@categoryName" required="yes">
        <cfargument type="string" name="imageName" dbvarname="@imageName" required="yes">
        <cfargument type="string" name="imagePrefix" dbvarname="@imagePrefix" required="yes">
        <cfset results = "OK">  
        <cftry>
            <cfstoredproc procedure="dbo.ImageProtal_NewCategory" datasource="#APPLICATION.Datasource#" >         
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.categoryDescription#">
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.categoryName#">
                <cfprocparam
                  cfsqltype="cf_sql_blob"
                  value="#Arguments.image#"> 
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.imageName#">
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.imagePrefix#">
				<cfprocresult name="qry_NeweCategory">     
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  
            </cfcatch>
		</cftry>	
		<cfreturn results />
	</cffunction> 
        
 <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->  
	<cffunction name="UpdateCategory" returntype="string" output="false"  access="public" description="Update Category">
        <cfargument type="string" name="categoryGuid" dbvarname="@categoryGuid" required="yes">
        <cfargument type="string" name="categoryDescription" dbvarname="@categoryDescription" required="yes">
        <cfargument type="string" name="categoryName" dbvarname="@categoryName" required="yes">
        <cfset results = "OK">  
        <cftry>
            <cfstoredproc procedure="dbo.ImageProtal_UpdateCategory" datasource="#APPLICATION.Datasource#" >         
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.categoryGuid#">                   
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.categoryDescription#">
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.categoryName#">
				<cfprocresult name="qry_UpdateCategory">     
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  
            </cfcatch>
		</cftry>	
		<cfreturn results />
	</cffunction> 
    
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->     
  	<cffunction name="DisplayProductOrder" output="false" returntype="query" access="public" description="Return products and sort order by category">
       <cfargument type="string" name="categoryGuid" dbvarname="@categoryGuid" required="yes">
       <cftry>
            <cfstoredproc procedure="[dbo].[ImageProtal_SelectProductOrder]" datasource="#APPLICATION.Datasource#" >                            
                <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        value="#Arguments.categoryGuid#">                   
               <cfprocresult name="qry_DisplayProductOrder">     
            </cfstoredproc>	
            <cfcatch type="Database"></cfcatch>
        </cftry>
        <cfreturn qry_DisplayProductOrder />
    </cffunction> 
       
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->     
  	<cffunction name="UpdateProductOrder" output="false" returntype="string" access="public" description="Return products and sort order by category">
        <cfargument type="string" name="categoryGuid" dbvarname="@categoryGuid" required="yes">
        <cfargument type="string" name="orderPair" dbvarname="@sortOrders" required="yes">
        <cfset results = "OK">  
        <cftry>
            <cfstoredproc procedure="[dbo].[ImageProtal_UpdateProductOrder]" datasource="#APPLICATION.Datasource#" >                            
                <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        value="#Arguments.categoryGuid#">                   
                <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        value="#Arguments.orderPair#">                   
               	<cfprocresult name="qry_UpdateProductOrder">     
            </cfstoredproc>	
            <cfcatch type="Database">
                <cfset results = "#cfcatch.detail#">  
            </cfcatch>
        </cftry>
        <cfreturn results />
    </cffunction>  

   <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->     
  	<cffunction name="DisplayCategoryOrder" output="false" returntype="query" access="public" description="Return category and sort order">
        <cftry>
            <cfstoredproc procedure="dbo.ImageProtal_SelectCategoryOrder" datasource="#APPLICATION.Datasource#" >                                               
               <cfprocresult name="qry_SelectCategoryOrder">     
            </cfstoredproc>	
            <cfcatch type="Database"></cfcatch>
        </cftry>
        <cfreturn qry_SelectCategoryOrder />
    </cffunction>    
      
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->     
  	<cffunction name="UpdateCategoryOrder" output="false" returntype="string" access="public" description="Update category sort order">
        <cfargument type="string" name="categoryGuid" dbvarname="@categoryGuid" required="yes">
        <cfargument type="string" name="orderPair" dbvarname="@sortOrders" required="yes">
        <cfset results = "OK">  
        <cftry>
            <cfstoredproc procedure="[dbo].[ImageProtal_UpdateCategoryOrder]" datasource="#APPLICATION.Datasource#" >                            
                <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        value="#Arguments.categoryGuid#">                   
                <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        value="#Arguments.orderPair#">                   
               	<cfprocresult name="qry_UpdateCategorytOrder">     
            </cfstoredproc>	
            <cfcatch type="Database">
                <cfset results = "#cfcatch.detail#">  
            </cfcatch>
        </cftry>
        <cfreturn results />
    </cffunction>  


   <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->     
  	<cffunction name="DisplayLinkOrder" output="false" returntype="query" access="public" description="Return Link and sort order">
      <cfargument type="string" name="categoryGuid" dbvarname="@categoryGuid" required="yes">
      <cfargument type="string" name="product" dbvarname="@product" required="yes">
         <cftry>
            <cfstoredproc procedure="dbo.ImageProtal_SelectLinkOrder" datasource="#APPLICATION.Datasource#" >                                               
                <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        value="#Arguments.categoryGuid#">                   
                <cfprocparam
                        cfsqltype="cf_sql_integer"
                        value="#Arguments.product#">                   
               	<cfprocresult name="qry_DisplayLinkOrder">         
            </cfstoredproc>	
            <cfcatch type="Database"></cfcatch>
        </cftry>
        <cfreturn qry_DisplayLinkOrder />
    </cffunction>    
      
    <!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->     
  	<cffunction name="UpdateLinkOrder" output="false" returntype="string" access="public" description="Update Link sort order">
        <cfargument type="string" name="categoryGuid" dbvarname="@categoryGuid" required="yes">
        <cfargument type="string" name="orderPair" dbvarname="@sortOrders" required="yes">
      	<cfargument type="string" name="product" dbvarname="@product" required="yes">
        <cfset results = "OK">  
        <cftry>
            <cfstoredproc procedure="[dbo].[ImageProtal_UpdateLinkOrder]" datasource="#APPLICATION.Datasource#" >                            
                <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        value="#Arguments.categoryGuid#">                   
                <cfprocparam
                        cfsqltype="cf_sql_integer"
                        value="#Arguments.product#">                   
                <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        value="#Arguments.orderPair#">                   
               	<cfprocresult name="qry_UpdateLinkOrder">     
            </cfstoredproc>	
            <cfcatch type="Database">
                <cfset results = "#cfcatch.detail#"> 
            </cfcatch>
        </cftry>
        <cfreturn results />
    </cffunction>  

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->   
   <cffunction name="getDeletedCategories" output="false" returntype="query" access="public" description="Return all Categories with isActive equal to 0">
		<cftry>
			<cfstoredproc procedure="[dbo].[ImageProtal_GetDeletedCategories]" datasource="#APPLICATION.Datasource#" >                            
            	<cfprocresult name="qry_ImageProtal_GetDeletedCategories">     
			</cfstoredproc>	
            <cfcatch type="Database"></cfcatch>
		</cftry>
		<cfreturn qry_ImageProtal_GetDeletedCategories />
    </cffunction>
    
<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->   
   <cffunction name="getDeletedLinks" output="false" returntype="query" access="public" description="Return all links with isActive equal to 0">
        <cfargument type="string" name="categoryGuid" dbvarname="@categoryGuid" required="yes">
      	<cfargument type="string" name="product" dbvarname="@product" required="yes">
		<cftry>
			<cfstoredproc procedure="[dbo].[ImageProtal_GetDeletedLinks]" datasource="#APPLICATION.Datasource#" >                            
                <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        value="#Arguments.categoryGuid#">                   
                <cfprocparam
                        cfsqltype="cf_sql_integer"
                        value="#Arguments.product#">                   
            	<cfprocresult name="qry_ImageProtal_GetDeletedLinks">     
			</cfstoredproc>	
            <cfcatch type="Database"></cfcatch>
		</cftry>
		<cfreturn qry_ImageProtal_GetDeletedLinks />
    </cffunction>  
     
<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->   
   <cffunction name="getDeletedProducts" output="false" returntype="query" access="public" description="Return all products with isActive equal to 0">
        <cfargument type="string" name="categoryGuid" dbvarname="@categoryGuid" required="yes">
		<cftry>
			<cfstoredproc procedure="[dbo].[ImageProtal_GetDeletedProducts]" datasource="#APPLICATION.Datasource#" >                            
                <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        value="#Arguments.categoryGuid#">                                     
            	<cfprocresult name="qry_ImageProtal_GetDeletedProducts">     
			</cfstoredproc>	
            <cfcatch type="Database"></cfcatch>
		</cftry>
		<cfreturn qry_ImageProtal_GetDeletedProducts />
    </cffunction>    
      
<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->   
   <cffunction name="setCategoryToActive" output="false" returntype="string" access="public" description="Set category to Active">
        <cfargument type="string" name="categoryGuid" dbvarname="@categoryGuid" required="yes">
        <cfset results = "OK">  
		<cftry>
			<cfstoredproc procedure="[dbo].[ImageProtal_SetCategoryToActive]" datasource="#APPLICATION.Datasource#" >                            
                <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        value="#Arguments.categoryGuid#">                                     
            	<cfprocresult name="qry_ImageProtal_SetCategoryToActive">     
			</cfstoredproc>	
            <cfcatch type="Database">
                <cfset results = "#cfcatch.detail#"> 
            </cfcatch>
        </cftry>
        <cfreturn results />    
    </cffunction>    
      
<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->   
   <cffunction name="setLinkToActive" output="false" returntype="string" access="public" description="Set link to Active">
        <cfargument type="string" name="Guid" dbvarname="@guid" required="yes">
        <cfset results = "OK">  
		<cftry>
			<cfstoredproc procedure="[dbo].[ImageProtal_SetLinkToActive]" datasource="#APPLICATION.Datasource#" >                            
                <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        value="#Arguments.Guid#">                                                       
            	<cfprocresult name="qry_ImageProtal_SetLinkToActive">     
			</cfstoredproc>	
            <cfcatch type="Database">
                <cfset results = "#cfcatch.detail#"> 
            </cfcatch>
        </cftry>
        <cfreturn results />    
    </cffunction>         
      
<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->   
   <cffunction name="SetProductToIsActive" output="false" returntype="string" access="public" description="Set category to Active">
        <cfargument type="string" name="categoryGuid" dbvarname="@categoryGuid" required="yes">
       	<cfargument type="string" name="product" dbvarname="@product" required="yes">
       <cfset results = "OK">  
		<cftry>
			<cfstoredproc procedure="[dbo].[ImageProtal_SetProductToIsActive]" datasource="#APPLICATION.Datasource#" >                            
                <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        value="#Arguments.categoryGuid#">                                     
                <cfprocparam
                        cfsqltype="cf_sql_integer"
                        value="#Arguments.product#">                   
            	<cfprocresult name="qry_ImageProtal_SetProductToIsActive">     
			</cfstoredproc>	
            <cfcatch type="Database">
                <cfset results = "#cfcatch.detail#"> 
            </cfcatch>
        </cftry>
        <cfreturn results />    
    </cffunction>          
        
        
                     
    </cfcomponent>
    