USE [HME]
GO

/****** Object:  Table [dbo].[PurchaseRequisitionLineItems]    Script Date: 9/13/2016 8:26:38 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PurchaseRequisitionLineItems](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[LineItem] [nvarchar](25) NULL,
	[QuantityRequested] [int] NULL,
	[UMO] [nvarchar](25) NULL,
	[EstimatedCost] [money] NULL,
	[RequestedDate] [datetime] NULL,
	[ManufacturePinDescription] [nvarchar](150) NULL,
	[PurchasedQuantity] [int] NULL,
	[PurchasedCost] [money] NULL,
	[DockDate] [datetime] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_PurchaseRequisitionLineItems_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PurchaseRequisitionLineItems] ADD  CONSTRAINT [DF_PurchaseRequisitionLineItems_CreateDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[PurchaseRequisitionLineItems]  WITH CHECK ADD  CONSTRAINT [FK_PurchaseRequisitionLineItems_PurchaseRequisition] FOREIGN KEY([Key])
REFERENCES [dbo].[PurchaseRequisition] ([id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[PurchaseRequisitionLineItems] CHECK CONSTRAINT [FK_PurchaseRequisitionLineItems_PurchaseRequisition]
GO


