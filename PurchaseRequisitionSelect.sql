USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[PurchaseRequisitionSelect]    Script Date: 9/13/2016 8:23:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[PurchaseRequisitionSelect](
	@id INT = NULL,
	@type NVARCHAR(10) = NULL,
	@purchaseOrder NVARCHAR(50) = NULL,
	@supplier NVARCHAR(50) = NULL
) AS

DECLARE
	@select NVARCHAR(4000),
	@from	NVARCHAR(4000),
	@where	NVARCHAR(4000),
	@sql	NVARCHAR(4000)

IF @id IS NOT NULL 
	BEGIN
		IF @type IS NOT NULL AND @type = 'order'
			BEGIN
				SELECT 
					pr.id AS [key], 
					pr.Supplier AS supplier_edit, 
					pr.Shipper AS shipping_edit, 
					pr.Requestor AS requestor,
					pr.RequestedDate AS requestor_date,
					pr.AccountNumber as account_number,
					pr.Department AS dept,
					pr.PurchaseOrder AS po,
					pr.Buyer AS buyer,
					pr.ShipVia AS shipvia,
					pr.comments
				FROM PurchaseRequisition pr 
				WHERE pr.id = @id
			END
		ELSE
			BEGIN
				SELECT 
					li.id,
					li.[Key],
					li.LineItem AS lineItem,
					li.QuantityRequested AS quantity,
					li.UMO AS umo,
					li.EstimatedCost AS estcost,
					li.RequestedDate AS needdate,
					li.ManufacturePinDescription AS manufacturedesc,
					li.PurchasedQuantity AS purchasequantity,
					li.PurchasedCost AS price,
					li.DockDate AS dockdate	
				FROM PurchaseRequisitionLineItems li 
				WHERE li.[key] = @id
			END
	END
ELSE 
	BEGIN
		SET @select = 'SELECT pr.id, pr.PurchaseOrder, pr.Requestor, pr.RequestedDate, li.[key], li.ManufacturePinDescription'
		SET @from =  ' FROM PurchaseRequisition pr INNER JOIN PurchaseRequisitionLineItems li ON  li.[Key] =  pr.id'
		SET @where = ' WHERE 1=1'

		IF @supplier IS NOT NULL
			BEGIN
				SET @where = @where + ' AND Supplier LIKE ' + '''%' + @Supplier + '%'''
			END
		IF @purchaseOrder IS NOT NULL
			BEGIN
				SET @where = @where + ' AND PurchaseOrder LIKE ' + '''%' + @purchaseOrder + '%'''
			END

		SET @sql = @select + @from + @where

		print @sql
		EXEC sp_executesql @sql
	END