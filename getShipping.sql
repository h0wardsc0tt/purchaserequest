USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[getShipping]    Script Date: 9/13/2016 8:24:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[getShipping](
	@key NVARCHAR(10)
)AS
SELECT 
       [street]
      ,[city]
      ,[state_code]
      ,[zip]
FROM [HME].[dbo].[customer_address]
WHERE key_customer_address = @key