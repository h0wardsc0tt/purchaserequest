USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[getCustomer]    Script Date: 9/13/2016 8:24:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[getCustomer](
	@lookup NVARCHAR(150)
)AS

select top 25 a.key_customer_address as value, i.first_name + ' ' + i.middle_initial + ' ' + i.last_name as label from [HME].[dbo].[customer] c
inner join [HME].[dbo].[customer_info] i on i.[key_customer_info] = c.key_billing_customer_info
inner join [HME].[dbo].[customer_address] a on a.key_customer_address = c.key_bill_address
where i.first_name + ' ' + i.middle_initial + ' ' + i.last_name like @lookup + '%'
order by i.first_name