<a href="##" class="showDropdown" style="position:absolute; top:-20px;">&nbsp;</a>
<div style="position:absolute; height:0;" class="expandable-element">
    <div class="ui-widget autocomplete-widget" style=" position:relative;">
        <div align="right" style="padding-right:10px;" >
            <a class="closeSearch" href="##"><i class="fa fa-times"></i></a>
         </div>
        <input id="tags" class="lookup-keypress supplier-shipping-autocomplete" placeholder="Lookup HME Suppliers..." style="width:90%;">
            <a href="##" class="searchClick"><i class="searchIcon" style="position:absolute;">select</i></a>
        <div style="text-align:center; width:100%;height:0;" class="recovery-content-row expandable-element error-message"> 
            <div class="system-message"></div>
        </div>            
    </div>
</div>        	
<div class="supplier-shipping-info" style="width:100%;">
    <div class="header" style="display:inline-block; font-size:11px; font-weight:700;">&nbsp;</div>
    <div class="save-cancel" style="float:right; padding-right:5px; display:none;">                    
        <a class="edit-save" href="##" style="color:#198009;">save</a>
        <a class="edit-cancel" href="##" style="color:#F10C0F;">cancel</a>
    </div>
</div>
