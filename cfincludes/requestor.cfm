<cfoutput>
<div class="row">
    <div class="row-height">
        <div class="col-xs-5 col-sm-5 col-sm-height" style="border-top:0;">
            <div class="inside inside-full-height" style="height:95%;">
                <div class="content" style="height:95%;">            	
                    <div style="width:100%;">
                        <div style="display:inline-block; width:65%;">REQUESTOR</div>
                        <div style="float:right; padding-right:5px; width:33%;">DATE:</div>
                    </div>
                    <div style="width:100%;">
                        <input id="requestor" type="search" value="#purchaseOrderObj.order.requestor#" placeholder="REQUESTER..." class="text-box-update pr-item" style="width:63%;">
                        <div style="float:right; padding-right:5px; width:33%;">
                            <input id="requestor_date" type="search" value="#formatdate(purchaseOrderObj.order.requestor_date,'mm/dd/yyyy')#" class="datepicker text-box-update pr-item" style="width:99%; text-align:center;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-3 col-sm-3 col-sm-height" style="border-top:0; border-left:0;">
            <div class="inside inside-full-height" style="height:100%;">
                <div class="content">            	
                    <div style="width:100%;">
                        <div style="display:inline-block; width:65%;">APPROVAL:</div>
                        <div style="float:right; padding-right:5px; width:35%;">DATE:</div>
                    </div>
                    <div style="width:100%;">
                        <input type="search" style="width:73%; visibility:hidden">
                        <div style="float:right; padding-right:5px; width:23%;">
                            <input type="search" class="datepicker" style="width:99%; text-align:center; visibility:hidden">
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-xs-2 col-sm-2 col-sm-height" style="border-top:0; border-left:0;">
            <div class="inside inside-full-height" style="height:95%;">
                <div class="content" style="height:95%;">            	
                    <div style="width:100%;">
                        <div style="display:inline-block; width:75%;">ACCT NO:</div>

                    </div>
                    <div style="width:100%;">
                        <input id="account_number" type="search"  value="#purchaseOrderObj.order.account_number#" placeholder="ACCT NO..." class="text-box-update pr-item" style="width:100%;">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-2 col-sm-2 col-sm-height" style="border-top:0; border-left:0;">
            <div class="inside inside-full-height">
                <div class="content" style="height:95%;">            	
                    <div style="width:100%;">
                        <div style="display:inline-block;">DEPT:</div>

                    </div>
                    <div>
                       <input id="dept" type="search"  value="#purchaseOrderObj.order.dept#" placeholder="DEPT..." class="text-box-update pr-item" style="width:100%;">
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div> 
</cfoutput>