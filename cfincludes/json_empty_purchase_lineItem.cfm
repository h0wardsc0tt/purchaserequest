<cfscript>
/*
line_item_clone = '';
line_item_clone = line_item_clone & '{"id":"", "status":"new", "key":"", "line_item":';
line_item_clone = line_item_clone & '[';
line_item_clone = line_item_clone & 	'  {"id":"lineItem", "value":"", "status":"", "type":"string"}';
line_item_clone = line_item_clone & 	', {"id":"quantity", "value":"", "status":"", "type":"int"}';
line_item_clone = line_item_clone & 	', {"id":"umo", "value":"", "status":"", "type":"string"}';
line_item_clone = line_item_clone & 	', {"id":"estcost", "value":"", "status":"", "type":"decimal"}';
line_item_clone = line_item_clone & 	', {"id":"needdate", "value":"", "status":"", "type":"date"}';
line_item_clone = line_item_clone & 	', {"id":"manufacturedesc", "value":"", "status":"", "type":"string"}';
line_item_clone = line_item_clone & 	', {"id":"purchasequantity", "value":"", "status":"", "type":"int"}';
line_item_clone = line_item_clone & 	', {"id":"price", "value":"", "status":"", "type":"decimal"}';
line_item_clone = line_item_clone & 	', {"id":"dockdate", "value":"", "status":"", "type":"date"}';
line_item_clone = line_item_clone & ']}';
*/

line_item_clone = '';
line_item_clone = line_item_clone & '{"id":"", "status":"new", "key":"", "line_item":';

line_item_clone = line_item_clone & 	'{"lineItem":""';
line_item_clone = line_item_clone & 	',"quantity":""';
line_item_clone = line_item_clone & 	',"umo":""';
line_item_clone = line_item_clone & 	',"estcost":""';
line_item_clone = line_item_clone & 	',"needdate":""';
line_item_clone = line_item_clone & 	',"manufacturedesc":""';
line_item_clone = line_item_clone & 	',"purchasequantity":""';
line_item_clone = line_item_clone & 	',"price":""';
line_item_clone = line_item_clone & 	',"dockdate":""}';
line_item_clone = line_item_clone & '}';

new_purchase_order = '';
new_purchase_order = new_purchase_order & '{"supplier_edit":""';
new_purchase_order = new_purchase_order & ',"shipping_edit":""';
new_purchase_order = new_purchase_order & ',"requestor":""';
new_purchase_order = new_purchase_order & ',"requestor_date":""';
new_purchase_order = new_purchase_order & ',"account_number":""';
new_purchase_order = new_purchase_order & ',"dept":""';
new_purchase_order = new_purchase_order & ',"po":""';
new_purchase_order = new_purchase_order & ',"buyer":""';
new_purchase_order = new_purchase_order & ',"shipvia":""';
new_purchase_order = new_purchase_order & ',"comments":""}';

new_purchase_req = "";
new_purchase_req = new_purchase_req & '{"key":"","status":"new", "order_change":"", "line_item_change":"",';
new_purchase_req = new_purchase_req & '"order":' & new_purchase_order;
/*
new_purchase_req = new_purchase_req & '[{"id":"supplier_edit","value":"", "status":"", "type":"string"},';
new_purchase_req = new_purchase_req & '{"id":"shipping_edit","value":"", "status":"", "type":"string"},';
new_purchase_req = new_purchase_req & '{"id":"requestor","value":"", "status":"", "type":"string"},';
new_purchase_req = new_purchase_req & '{"id":"requestor_date","value":"", "status":"", "type":"date"},';
new_purchase_req = new_purchase_req & '{"id":"account_number","value":"", "status":"", "type":"string"},';
new_purchase_req = new_purchase_req & '{"id":"dept","value":"", "status":"", "type":"string"},';
new_purchase_req = new_purchase_req & '{"id":"po","value":"", "status":"", "type":"string"},';
new_purchase_req = new_purchase_req & '{"id":"buyer","value":"", "status":"", "type":"string"},';
new_purchase_req = new_purchase_req & '{"id":"shipvia","value":"", "status":"", "type":"string"},';
new_purchase_req = new_purchase_req & '{"id":"comments","value":"", "status":"", "type":"string"}]';
*/

new_purchase_req = new_purchase_req & ',"line_items":[' & line_item_clone & ']}';

//WriteOutput(new_purchase_req);
/*
new_purchase_req = "";
new_purchase_req = new_purchase_req & '{"key":"","status":"new", "order_change":"", "line_item_change":"",';
new_purchase_req = new_purchase_req & '"order":';
new_purchase_req = new_purchase_req & '[{"id":"supplier_edit","value":"", "status":"", "type":"string"},';
new_purchase_req = new_purchase_req & '{"id":"shipping_edit","value":"", "status":"", "type":"string"},';
new_purchase_req = new_purchase_req & '{"id":"requestor","value":"", "status":"", "type":"string"},';
new_purchase_req = new_purchase_req & '{"id":"requestor_date","value":"", "status":"", "type":"date"},';
new_purchase_req = new_purchase_req & '{"id":"account_number","value":"", "status":"", "type":"string"},';
new_purchase_req = new_purchase_req & '{"id":"dept","value":"", "status":"", "type":"string"},';
new_purchase_req = new_purchase_req & '{"id":"po","value":"", "status":"", "type":"string"},';
new_purchase_req = new_purchase_req & '{"id":"buyer","value":"", "status":"", "type":"string"},';
new_purchase_req = new_purchase_req & '{"id":"shipvia","value":"", "status":"", "type":"string"},';
new_purchase_req = new_purchase_req & '{"id":"comments","value":"", "status":"", "type":"string"}]';

new_purchase_req = new_purchase_req & ',"line_items":[{"id":"itemRow_1", "status":"", "key":"", "line_item":[';
new_purchase_req = new_purchase_req & '{"id":"lineItem", "value":"", "status":"", "type":"string"},';
new_purchase_req = new_purchase_req & '{"id":"quantity", "value":"", "status":"", "type":"int"},';
new_purchase_req = new_purchase_req & '{"id":"umo", "value":"", "status":"", "type":"string"},';
new_purchase_req = new_purchase_req & '{"id":"estcost", "value":"", "status":"", "type":"decimal"},';
new_purchase_req = new_purchase_req & '{"id":"needdate", "value":"", "status":"", "type":"date"},';
new_purchase_req = new_purchase_req & '{"id":"manufacturedesc", "value":"", "status":"", "type":"string"},';
new_purchase_req = new_purchase_req & '{"id":"purchasequantity", "value":"", "status":"", "type":"int"},';
new_purchase_req = new_purchase_req & '{"id":"price", "value":"", "status":"", "type":"decimal"},';
new_purchase_req = new_purchase_req & '{"id":"dockdate", "value":"", "status":"", "type":"date"}]}';
new_purchase_req = new_purchase_req & ']}';
*/
</cfscript>
