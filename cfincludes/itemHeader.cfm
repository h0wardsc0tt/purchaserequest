<div class="row">
    <div class="line-item-header" style="display:inline-block;width:73%; border-right: solid 1px #000000;">
    REQUESTOR
    </div>
    <div class="line-item-header" style="display:inline-block;width:25%;">
    PURCHASING
    </div>
</div>
<div class="row" style="background-color:#ecf8fc; color:#4a889f;">
    <div class="line-item-col-header" style="display:inline-block;width:11%;">
    LINE ITEM
    </div>
    <div class="line-item-col-header" style="display:inline-block;width:5%;">
    QTY 
    </div>
    <div class="line-item-col-header" style="display:inline-block;width:10%;">
    UOM 
    </div>
    <div class="line-item-col-header" style="display:inline-block;width:10%;">
    EST. COST
    </div>
    <div class="line-item-col-header"  style="display:inline-block;width:10%;">
    NEED DATE
    </div>
    <div class="line-item-col-header"  style="display:inline-block;width:25%;">
    MANUFACTURE PN-DESCRIPTION<br />(Avante PN where applicable)
    </div>
    <div class="line-item-col-header" style="display:inline-block;width:5%;" >
    QTY 
    </div>
    <div class="line-item-col-header"  style="display:inline-block;width:10%;">
    PRICE 
    </div>
    <div class="line-item-col-header"  style="display:inline-block;width:10%;">
    DOCK DATE
    </div>
</div>