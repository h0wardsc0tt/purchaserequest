<cfoutput>
<div class="row" style="font-size:12px;">  
Requisitions for computer hardware or software must be approved by an IT Representative.  All purchase requisitions for depreciated capital equipment must be approved by General Accounting and an Executive Staff Member, usually the one responsible for the area where the item is needed.
</div>
<div  class="row" style="min-height:50px;padding-bottom:0px; border:solid 1px ##000000;"> 
    COMMENTS:
    <textarea id="comments" class="content-edit text-box-update" style="height:90px; border:0; width:100%; 
    resize: none; padding:0;">#purchaseOrderObj.order.comments#</textarea>
</div>
<div class="row" style="font-size:10px;">  
    HME 56016B(3/12)
</div>   
</cfoutput>