<cfoutput>
		<div class="row-height" style="height:190px;">
			<div class="col-xs-5 col-sm-5 col-sm-height" style="padding:0">
               <div class="inside inside-full-height">
                    <div id="supplier_info" class="content" style=" position:relative;">
                    	<cfinclude template="supplier_edit.cfm"> 
                        <div class="supplier-shipping-content">
                        	<div class="content-print-header" 
                            	style="display:block; font-size:11px; font-weight:700;">SUPPLIER (Name, Address, Phone, email, website</div>
                                <textarea id="supplier_edit" class="content-edit text-box-update" style="height:165px; width:100%; 
                                resize: none; padding:0;">#ReplaceNoCase(purchaseOrderObj.order.supplier_edit,"\n",Chr(13) & Chr(10), "ALL")#</textarea>
                        </div> 
                    </div>
                </div>
      		</div>
     		<div class="col-xs-5 col-sm-5 col-sm-height" style="border-left:0;padding:0;">
                <div class="inside inside-full-height">
                    <div id="ship_to_info" class="content" style="position:relative;">            	
                        <!---<cfinclude template="./cfincludes/shipping_edit.cfm">--->
                        <div class="supplier-shipping-content">
                        	<div class="content-print-header" 
                        		style="display:block; font-size:11px; font-weight:700;">SHIP TO/DELIVER TO: (Name, Address, Phone, email)</div>
                                <textarea id="shipping_edit" class="content-edit text-box-update" style="height:165px; width:100%; 
                                resize: none; padding:0;">#ReplaceNoCase(purchaseOrderObj.order.shipping_edit,"\n",Chr(13) & Chr(10), "ALL")#</textarea>
                        </div> 
                    </div>
                </div>
      		</div>
			<cfinclude template="po.cfm">
		</div>
</cfoutput>