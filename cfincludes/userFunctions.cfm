<cfscript> 
function get_requested_pr(prId) { 
  	object = CreateObject("component", "_cfcs.productReq"); 
	serializer = new lib.JsonSerializer();
	
	pr_Struct =  StructNew();
	pr_Struct['id'] = prId;
	pr_Struct['type'] = 'order';
	
	li_Struct =  StructNew();
	li_Struct['type'] = 'lineItem';
		
	returnSelectResult = object.PurchaseRequisitionSelect(
		argumentCollection=pr_Struct
	);
	
	json = serializer.serialize(returnSelectResult);
	
	returnSelectResultObj = DeserializeJSON(json);
	order = returnSelectResultObj[1];
	
	key = order.key;
	li_Struct['id'] = key;
	
	returnLineItems = object.PurchaseRequisitionSelect(
		argumentCollection=li_Struct
	);
	
	json = serializer.serialize(returnLineItems);
	returnlineItemsObj = DeserializeJSON(json);
	
	StructDelete(order,"key");
	
	purchase_order = '';
	purchase_order = purchase_order & '{"key":"","status":"", "order_change":"", "line_item_change":"",';
	//purchase_order = purchase_order & '"order":[]';
	purchase_order = purchase_order & '"order":""';
	purchase_order = purchase_order & ',"line_items":[]}';
	
	purchase_orderObj = DeserializeJSON(purchase_order);
	purchase_orderObj.key = key;
	
	line_item = '{"id":"", "status":"", "key":"", "line_item":""}';
	line_itemObj = DeserializeJSON(line_item);
		
	for(i = 1; i LTE ArrayLen(returnlineItemsObj); i++){
		line_itemObj = DeserializeJSON(line_item);
		line_itemObj.key = returnlineItemsObj[i].id;
		line_itemObj.id = 'itemRow_' & i;
		StructDelete(returnlineItemsObj[i],"key");
		StructDelete(returnlineItemsObj[i],"id");
		//arrayAppend(line_itemObj.line_item, returnlineItemsObj[i], true);
		arrayAppend(purchase_orderObj.line_items, line_itemObj, true);
		line_itemObj.line_item = returnlineItemsObj[i];
	 }
	
	//arrayAppend(purchase_orderObj.order, order, true);
	purchase_orderObj.order =  order;
	return replace(serializer.serialize(purchase_orderObj),"\n","\\n","all");

}  

function formatdate(date, format) { 
if(date == '')
	return '';
return dateFormat(date,format);
}

</cfscript> 

