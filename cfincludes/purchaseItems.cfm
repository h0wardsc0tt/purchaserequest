<cfoutput>
<div id="itemRows">   
    <div id="itemRow_1" class="row items"  style="border:solid 1px ##000000; padding: 3px 0px 3px 0px">
        <div class=" line-item"  style="text-align:center; display:inline-block; width:11%; padding:0;">
            <i class="fa fa-plus add-row" style="float:left; padding-top:7px;" 
                onClick="newLineItem();"></i>
            <input itemid="lineitem" type="search" value="#purchase_req_obj['line_items'][1]['line_item'][1]['value']#" class="text-box-update line-item" style="width:80%; text-align:center;">
                <i class="fa fa-times" style="float:right; padding-top:7px;" 
                onClick="deleteRowRequest($(this).parent().parent());"></i>
            </div>
        <div class=" line-item" style="display:inline-block; width:5%; padding:0;" >
        <input itemid="quantity" type="search" value="#purchase_req_obj['line_items'][1]['line_item'][2]['value']#" class="text-box-update line-item numeric" style="width:100%;text-align:center;">
        </div>
        <div class="line-item"  style="display:inline-block; width:10%; padding:0;">
        <input itemid="umo" type="search" value="#purchase_req_obj['line_items'][1]['line_item'][3]['value']#" class="text-box-update line-item" style="width:100%;text-align:center;">
        </div>
        <div class=" line-item"  style="display:inline-block; width:10%; padding:0;">
        <input itemid="estcost" type="search" value="#purchase_req_obj['line_items'][1]['line_item'][4]['value']#" class="text-box-update line-item numeric" style="width:100%;text-align:center;">
        </div>
        <div class=" line-item" style="display:inline-block; width:10%; padding:0;">
        <input itemid="needdate" type="search" value="#purchase_req_obj['line_items'][1]['line_item'][5]['value']#" class="datepicker text-box-update line-item date" itemid="needdate" 
        	style="width:99%;text-align:center;">
        </div>
        <div class=" line-item"  style="display:inline-block; width:25%; padding:0;">
        <input itemid="manufacturedesc" type="search" value="#purchase_req_obj['line_items'][1]['line_item'][6]['value']#" class="text-box-update line-item" style="width:100%;">
        </div>
        <div class=" line-item"  style="display:inline-block; width:5%; padding:0;">
        <input itemid="purchasequantity" type="search" value="#purchase_req_obj['line_items'][1]['line_item'][7]['value']#" class="text-box-update line-item numeric" style="width:100%;text-align:center;">
        </div>
        <div class=" line-item"  style="display:inline-block; width:10%; padding:0;">
        <input itemid="price" type="search" value="#purchase_req_obj['line_items'][1]['line_item'][8]['value']#" class="text-box-update line-item numeric" style="width:100%;text-align:center;">
        </div>
        <div class=" line-item" style="display:inline-block; width:10%; padding:0;" >
        <input itemid="dockdate" type="search" value="#purchase_req_obj['line_items'][1]['line_item'][9]['value']#" class="datepicker text-box-update line-item date" style="width:100%;text-align:center;">
        </div>
    </div>
    <!---
    <div id="itemRow_2" class="row items">
        <div class="col-xs-1 line-item"  style="text-align:center;">
            <i class="fa fa-plus add-row" style="float:left; padding-top:7px; visibility:hidden;" 
                onClick="deleteRowRequest($(this).parent().parent());"></i>
            <input itemid="lineItem" type="search" class="text-box-update line-item numeric" style="width:43%;">
                <i class="fa fa-times" style="float:right; padding-top:7px;" 
                onClick="deleteRowRequest($(this).parent().parent());"></i>
            </div>
        <div class="col-xs-1 line-item" >
        <input itemid="quantity" type="search" class="text-box-update line-item numeric" style="width:99%;">
        </div>
        <div class="col-xs-1 line-item" >
        <input itemid="umo" type="search" class="text-box-update line-item" style="width:99%;">
        </div>
        <div class="col-xs-1 line-item" >
        <input itemid="estcost" type="search" class="text-box-update line-item numeric" style="width:99%;">
        </div>
        <div class="col-xs-1 line-item">
        <input itemid="needdate" type="search" class="datepicker text-box-update line-item" itemid="needdate" 
        	style="width:99%;text-align:center;">
        </div>
        <div class="col-xs-4 line-item" >
        <input itemid="manufacturedesc" type="search" class="text-box-update line-item" style="width:99%;">
        </div>
        <div class="col-xs-1 line-item" >
        <input itemid="purchasequantity" type="search" class="text-box-update line-item numeric" style="width:99%;">
        </div>
        <div class="col-xs-1 line-item" >
        <input itemid="price" type="search" class="text-box-update line-item numeric" style="width:99%;">
        </div>
        <div class="col-xs-1 line-item" >
        <input itemid="dockdate" type="search" class="datepicker text-box-update line-item" style="width:99%;">
        </div>
    </div>
	--->
   
</div>
</cfoutput>