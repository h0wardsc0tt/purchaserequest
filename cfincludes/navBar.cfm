<nav class="navbar navbar-inverse navbar-fixed-top navbar-custom">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand navbar-brand-hme-image nav-logo" href="#"><img src="../images/hme-logo.png" alt="HME Logo" class="img-responsive"></a> </div>
    <div id="navbar" class="navbar-collapse collapse">
	  <ul class="nav navbar-nav navbar-right">
      <li style="padding-top:8px;padding-right:10px;"><button id="search_purchase_req" onclick="toggle_modal($('#searchModal'));" type="button" class="btn btn-primary" value="Submit" >Search</button></li>
      <li style="padding-top:8px;padding-right:10px;"><button id="pfd_purchase_req" onclick="print_pr();" type="button" class="btn btn-primary" value="Submit" >PDF</button></li>
      <li style="padding-top:8px;padding-right:10px;"><button id="delete_purchase_req" onclick="submit_pr('delete');" type="button" class="btn btn-primary" value="Submit" >Delete</button></li>
      <li style="padding-top:8px;padding-right:10px;"><button id="new_purchase_req" onclick="new_pr();" type="button" class="btn btn-primary" value="Submit" >New</button></li>
      <li style="padding-top:8px;padding-right:10px;"><button id="save_purchase_req" disabled=true onclick="submit_pr('update');" type="button" class="btn btn-primary" value="Submit" >Save</button></li>
      <li style="padding-top:8px;padding-right:10px; display:none;"><button id="cancel_purchase_req" onclick="cancel_pr();" type="button" class="btn btn-default" value="Submit" >Cancel</button></li>
		<li><a href="admin.cfm?pg=Login">Login</a></li>
	  </ul>
      <form method="post" class="navbar-form navbar-right" id="SearchForm" style="display:none;">
      	<div class="search">
      		<span class="fa fa-search" onClick="$('#SearchForm').submit();"></span>
        	<input type="search" id="Search" name="Search" class="form-control" placeholder="Search..." >
		</div>
		<input type="hidden" name="submitted" value="true" />
      </form>
    </div>
  </div>
</nav>