USE [HME]
GO

/****** Object:  Table [dbo].[PurchaseRequisition]    Script Date: 9/13/2016 8:26:03 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PurchaseRequisition](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[PurchaseOrder] [nvarchar](25) NULL,
	[Buyer] [nvarchar](25) NULL,
	[ShipVia] [nvarchar](25) NULL,
	[Supplier] [nvarchar](500) NULL,
	[Shipper] [nvarchar](500) NULL,
	[Requestor] [nvarchar](25) NULL,
	[RequestedDate] [datetime] NULL,
	[AccountNumber] [nvarchar](25) NULL,
	[Department] [nvarchar](25) NULL,
	[Comments] [nvarchar](4000) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_PurchaseRequisition] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PurchaseRequisition] ADD  CONSTRAINT [DF_PurchaseRequisition_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[PurchaseRequisition]  WITH CHECK ADD  CONSTRAINT [FK_PurchaseRequisition_PurchaseRequisition] FOREIGN KEY([id])
REFERENCES [dbo].[PurchaseRequisition] ([id])
GO

ALTER TABLE [dbo].[PurchaseRequisition] CHECK CONSTRAINT [FK_PurchaseRequisition_PurchaseRequisition]
GO


