<cfsetting showdebugoutput="no">

<cfparam name="User_IsAtLogin" default="false">
<cfparam name="User_IsLoggedIn" default="false">
<cfparam name="URL.pg" default="">
<cfparam name="URL.st" default="">

<cfif CGI.QUERY_STRING CONTAINS "Login">
	<cfset User_IsAtLogin = true>
</cfif>

<cfif StructKeyExists(SESSION,"IsLoggedIn")>
	<cfset User_IsLoggedIn = SESSION.IsLoggedIn>
</cfif>




<cfinclude template="./_tmp_HTML_Header.cfm">
<strong></strong>
<cfswitch expression="#URL.pg#">
    <cfcase value="Login">
        <cfinclude template="./security/_dat_login_prep.cfm">
        <cfswitch expression="#URL.st#">
            <cfcase value="Validate">
                <cfinclude template="./security/_tmp_login_check.cfm">
            </cfcase>
            
            <cfdefaultcase>
                <cfinclude template="./security/_tmp_login_form.cfm">
            </cfdefaultcase>
        </cfswitch>
    </cfcase>
    <cfcase value="Logout">
    	<cfinclude template="./security/_tmp_logout.cfm">
    </cfcase>
    <cfcase value="ManageManuals">
        <cfif User_IsLoggedIn>
            <cfinclude template="./_dat_manual_prep.cfm">
            <cfswitch expression="#URL.st#">
                <cfcase value="Edit">
                    <cfinclude template="./_tmp_manual_edit.cfm">
                </cfcase>
                <cfdefaultcase>
                    <cfinclude template="./_tmp_manual_view.cfm">
                </cfdefaultcase>
            </cfswitch>
        <cfelse>
        	<cflocation url="./?pg=Login" addtoken="no">
        </cfif>
    </cfcase>
    <cfdefaultcase>
    	<cfif User_IsLoggedIn>
        	<cflocation url="./?pg=ManageManuals" addtoken="no">
        <cfelse>
    		<cflocation url="./?pg=Login" addtoken="no">
        </cfif>
    </cfdefaultcase>
</cfswitch>

<cfinclude template="./_tmp_HTML_Footer.cfm">