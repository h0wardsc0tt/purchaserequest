<cfscript>
purchase_req = '';
purchase_req = purchase_req & '{"key":"","status":"", "order_change":"", "line_item_change":"",';


							purchase_req = purchase_req & '"order":';
							purchase_req = purchase_req & '[{"id":"supplier_edit","value":"SHI.com\\nChloe Frew\\n1501 S MoPac Expressway\\nSuite 400\\nAustin, TX 78746~Phone: 512-676-23156\\nFax: 512-676-2315\\nEmail: Chloe_Frew@shi.com", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"shipping_edit","value":"SHI.com\\nChloe Frew\\n1501 S MoPac Expressway\\nSuite 400\\nAustin, TX 78746~Phone: 512-676-23156\\nFax: 512-676-2315\\nEmail: Chloe_Frew@shi.com", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"requestor","value":"Eric Buclatin", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"requestor_date","value":"09/10/2016", "status":"", "type":"date"},';
							purchase_req = purchase_req & '{"id":"account_number","value":"22-555-44444", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"dept","value":"IT", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"po","value":"22-555-44444", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"buyer","value":"Buyer", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"shipvia","value":"Ship Via", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"comments","value":"No Comments", "status":"", "type":"string"}]';

							purchase_req = purchase_req & ',"line_items":[';
							purchase_req = purchase_req & '{"id":"itemRow_1", "status":"", "key":"", "line_item":[';
							purchase_req = purchase_req & '{"id":"lineItem", "value":"1", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"quantity", "value":"1", "status":"", "type":"int"},';
							purchase_req = purchase_req & '{"id":"umo", "value":"123-00", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"estcost", "value":"$1,00.00", "status":"", "type":"decimal"},';
							purchase_req = purchase_req & '{"id":"needdate", "value":"09/21/2016", "status":"", "type":"date"},';
							purchase_req = purchase_req & '{"id":"manufacturedesc", "value":"Head Set", "status":"", "type":"string"},';
							purchase_req = purchase_req & '{"id":"purchasequantity", "value":"1", "status":"", "type":"int"},';
							purchase_req = purchase_req & '{"id":"price", "value":"$1,000.00", "status":"", "type":"decimal"},';
							purchase_req = purchase_req & '{"id":"dockdate", "value":"09/18/2016", "status":"", "type":"date"}]}';
							/*
							purchase_req = purchase_req & 	',{"id":"itemRow_1", "status":"", "key":"", "info":[';
							purchase_req = purchase_req & 	'  {"id":"lineItem", "value":"", "status":"", "type":"int"}';
							purchase_req = purchase_req & 	', {"id":"quantity", "value":"", "status":"", "type":"int"}';
							purchase_req = purchase_req & 	', {"id":"umo", "value":"", "status":"", "type":"string"}';
							purchase_req = purchase_req & 	', {"id":"estcost", "value":"", "status":"", "type":"decimal"}';
							purchase_req = purchase_req & 	', {"id":"needdate", "value":"", "status":"", "type":"date"}';
							purchase_req = purchase_req & 	', {"id":"manufacturedesc", "value":"", "status":"", "type":"string"}';
							purchase_req = purchase_req & 	', {"id":"purchasequantity", "value":"", "status":"", "type":"int"}';
							purchase_req = purchase_req & 	', {"id":"price", "value":"", "status":"", "type":"decimal"}';
							purchase_req = purchase_req & 	', {"id":"dockdate", "value":"", "status":"", "type":"date"}]}';
							*/
							purchase_req = purchase_req & ']';

purchase_req = purchase_req & '}';


</cfscript>
<cfinclude template="./cfincludes/userFunctions.cfm">

<!---<cfset purchaseOrderObj = DeserializeJSON(get_requested_pr('31'))>--->
<cfset purchaseOrder = get_requested_pr('33')>
<cfset purchaseOrderObj = DeserializeJSON(purchaseOrder)>

<cfset purchase_req_obj = DeserializeJSON(#purchase_req#)>

<cfoutput>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=11" />

    <title>Purchase Requisition</title>
    <meta name="description" content="Bootstrap 3 responsive columns of same height">
    <!-- include bootstrap -->
	<link rel="stylesheet" href="css//bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
 	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">

	<!-- Custom styles for this template -->
    <link href="css/purchaseRequest.css" rel="stylesheet">
</head>
<body>

            <div class="container" style="width:98%;">
                  <ul class="nav navbar-nav navbar-right">

                  <li style="padding-top:8px;padding-right:10px;"><button id="pfd_purchase_req" onclick="print_pr();" type="button" class="btn btn-primary" value="Submit" >PDF</button></li>
                  <li style="padding-top:8px;padding-right:10px;"><button id="delete_purchase_req" onclick="submit_pr('delete');" type="button" class="btn btn-primary" value="Submit" >Delete</button></li>
                  <li style="padding-top:8px;padding-right:10px;"><button id="new_purchase_req" onclick="new_pr();" type="button" class="btn btn-primary" value="Submit" >New</button></li>
                  <li style="padding-top:8px;padding-right:10px;"><button id="save_purchase_req" disabled=true onclick="submit_pr('update');" type="button" class="btn btn-primary" value="Submit" >Save</button></li>
                  <li style="padding-top:8px;padding-right:10px; display:none;"><button id="cancel_purchase_req" onclick="cancel_pr();" type="button" class="btn btn-default" value="Submit" >Cancel</button></li>
                  </ul>
	<div id="container_row1" class="row">
	<cfinclude template="./cfincludes/orderform.cfm">
	</div>
	<cfinclude template="./cfincludes/requestor.cfm">
 	<cfinclude template="./cfincludes/itemHeader.cfm">
	<cfinclude template="./cfincludes/purchaseItems.cfm">
	<cfinclude template="./cfincludes/commentFooter.cfm">
 </div>
 

<cfinclude template="./cfincludes/deleteItemRowModal.cfm">
<cfinclude template="./cfincludes/purchase-req-searchModal.cfm">
<cfinclude template="./cfincludes/update-purchase-reqModal.cfm">   
<cfinclude template="./cfincludes/newLineItem.cfm">
<cfinclude template="./cfincludes/json_empty_purchase_lineItem.cfm">  

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<cfoutput>


<script type="text/javascript">
//var json = JSON.parse('#purchase_req#');
var json = JSON.parse('#purchaseOrder#');
var json_new_line_item  = JSON.parse('#line_item_clone#');
var json_new_purchase_req = JSON.parse('#new_purchase_req#');
//var json_get_pr = JSON.parse(#get_requested_pr('31')#);
//var json_get_pr = JSON.parse('#purchaseOrder#');

</script>
</cfoutput>
<script type="text/javascript" src="js/purchase-request.js"></script> 
<script type="text/javascript" src="js/autoComplete.js"></script> 
<script type="text/javascript" src="js/spin.min.js"></script>
<script type="text/javascript" src="js/spinnerOptions.js"></script> 
<script type="text/javascript" src="js/page_setup.js"></script> 
<script type="text/javascript" src="js/supplier_shipping.js"></script> 
<script type="text/javascript" src="js/jsonsql-0.1.js"></script> 
</body>
</html>

<cfscript>
/*
_purchase_req = deserializeJSON(purchase_req);

supplier = 1;
shipping = 2;
requestor = 3;
requestor_date = 4;
account_number = 5;
dept = 6;
po = 7;
buyer = 8;
shipvia = 9;
comments = 10;
*/
</cfscript>

<cfscript>
/*
	pr_Struct.id=#purchase_req_obj.key#;
	pr_Struct.purchaseOrder="#purchase_req_obj['order'][po]['value']#";
	pr_Struct.accountNumber="#purchase_req_obj.order[account_number]['value']#";

	object = CreateObject("component", "_cfcs.productReq"); 
	returnResult = object.updatePurchaseReq(argumentCollection=pr_Struct); 
*/
/*
returnResult="";
 	object = CreateObject("component", "_cfcs.productReq"); 
	returnResult = object.updatePurchaseReq(
		id = purchase_req_obj.key,
		purchaseOrder = "777777",
		buyer = "#purchase_req_obj.order[buyer]['value']#",
		shipVia = "#purchase_req_obj.order[shipvia]['value']#",
		supplier = "#purchase_req_obj.order[supplier]['value']#",
		shipper = "#purchase_req_obj.order[shipping]['value']#",
		requestor = "#purchase_req_obj.order[requestor]['value']#",
		requestedDate = "#purchase_req_obj.order[requestor_date]['value']#",
		accountNumber = "#purchase_req_obj.order[account_number]['value']#",
		department = "#purchase_req_obj.order[dept]['value']#",	
		comments = "#purchase_req_obj.order[comments]['value']#",
		type = "xdelete"
	); 

returnObj = DeserializeJSON(returnResult);
*/
/*
	for(i = 1; i LTE ArrayLen(purchase_req_obj.order); i++){
		//WriteOutput("categorydescription : " & purchase_req_obj.order[i].value & "<br>"); 
     }
	for(i = 1; i LTE ArrayLen(purchase_req_obj.line_items); i++){
		WriteOutput("categorydescription : " & purchase_req_obj.line_items[i].id & "<br>"); 
		for(j = 1; j LTE ArrayLen(purchase_req_obj.line_items[i].line_item); j++){
			WriteOutput("categorydescription : " & purchase_req_obj.line_items[i].line_item[j].value & "<br>"); 
		 }
	}
*/
	
/*
	returnResult = object.updatePurchaseReqLineitem(
		id = '',
		key = 99,
		lineItem = "33",
		quantity = 2,
		umo = "asdasdd fdfdfg df  dfg dfg ",
		estcost = 1000.00,
		needdate = "09/01/2016",
		manufacturedesc = "sadasd dfffff ghhh t g g ghghh",
		purchasequantity = 2,
		price = "1000.00",
		dockDate = "09/01/2016",
		type = "insert"
	); 
	*/	
/*
	lineItem = 1;
	quantity = 2;
	umo = 3;
	estcost = 4;
	needdate = 5;
	manufacturedesc = 6;
	purchasequantity = 7;
	price = 8;
	dockdate = 9;
	*/
/*
	for(i = 1; i LTE ArrayLen(purchase_req_obj.line_items); i++){
		//WriteOutput("categorydescription : " & purchase_req_obj.line_items[i].id & "<br>"); 
		WriteOutput("ArrayLength : " & ArrayLen(purchase_req_obj.line_items) & "<br>"); 
		//for(j = 1; j LTE ArrayLen(purchase_req_obj.line_items); j++){
				WriteOutput("#purchase_req_obj.line_items[i].key#" & "<br>");

				WriteOutput("#purchase_req_obj.line_items[i].line_item[lineItem].value#" & "<br>");
				WriteOutput("#purchase_req_obj.line_items[i].line_item[quantity].value#" & "<br>");
				WriteOutput("#purchase_req_obj.line_items[i].line_item[umo].value#" & "<br>");
				WriteOutput("#purchase_req_obj.line_items[i].line_item[estcost].value#" & "<br>");
				WriteOutput("#purchase_req_obj.line_items[i].line_item[needdate].value#" & "<br>");
				WriteOutput("#purchase_req_obj.line_items[i].line_item[manufacturedesc].value#" & "<br>");
				WriteOutput("#purchase_req_obj.line_items[i].line_item[purchasequantity].value#" & "<br>");
				WriteOutput("#purchase_req_obj.line_items[i].line_item[price].value#" & "<br>");
				WriteOutput("#purchase_req_obj.line_items[i].line_item[dockDate].value#" & "<br>");
		 
		//}
	}

*/	
/*
	for(i = 1; i LTE ArrayLen(purchase_req_obj.line_items); i++){
		WriteOutput("#purchase_req_obj.line_items[i].id#" & "<br>");
			returnResultLineitem = object.updatePurchaseReqLineItem(
				id = #purchase_req_obj.line_items[i].key#,
				key = 45,
				type = "xxx",
				lineItem = "#purchase_req_obj.line_items[i].line_item[lineItem].value#",
				quantity = #purchase_req_obj.line_items[i].line_item[quantity].value#,
				umo = "#purchase_req_obj.line_items[i].line_item[umo].value#",
				estcost = "#ReplaceNoCase(ReplaceNoCase(purchase_req_obj.line_items[i].line_item[estcost].value,'$',''),',','','ALL')#",
				needdate = "#purchase_req_obj.line_items[i].line_item[needdate].value#",
				manufacturedesc = "#purchase_req_obj.line_items[i].line_item[manufacturedesc].value#",
				purchasequantity = #purchase_req_obj.line_items[i].line_item[purchasequantity].value#,
				price = "#ReplaceNoCase(ReplaceNoCase(purchase_req_obj.line_items[i].line_item[price].value,'$',''),',','','ALL')#",
				dockDate = "#purchase_req_obj.line_items[i].line_item[dockDate].value#",
				lineItemId = "#purchase_req_obj.line_items[i].id#"
			); 			 
		returnLineItemObj = DeserializeJSON(returnResultLineitem);
		arrayAppend(returnObj.lineItems, returnLineItemObj, true);
	}
	return returnObj;
	*/	
object = CreateObject("component", "_cfcs.productReq"); 

serializer = new lib.JsonSerializer();

pr_Struct =  StructNew();
pr_Struct['id'] = '31';
pr_Struct['type'] = 'order';

li_Struct =  StructNew();
li_Struct['type'] = 'lineItem';
	
returnSelectResult = object.PurchaseRequisitionSelect(
	argumentCollection=pr_Struct
);

StructClear(pr_Struct);
pr_Struct =  StructNew();
pr_Struct['id'] = '';
pr_Struct['type'] = '';
pr_Struct['supplier'] = '';
pr_Struct['purchaseOrder'] = '8888';
pr_Struct['count'] = 0;


returnSelect = object.PurchaseRequisitionSelect(
	argumentCollection=pr_Struct
);

json = serializer.serialize(returnSelectResult);

returnSelectResultObj = DeserializeJSON(json);
order = returnSelectResultObj[1];

key = order.key;
li_Struct['id'] = key;

returnLineItems = object.PurchaseRequisitionSelect(
	argumentCollection=li_Struct
);

json = serializer.serialize(returnLineItems);
returnlineItemsObj = DeserializeJSON(json);

StructDelete(order,"key");

purchase_req = '';
purchase_req = purchase_req & '{"key":"","status":"", "order_change":"", "line_item_change":"",';
purchase_req = purchase_req & '"order":""';
purchase_req = purchase_req & ',"line_items":[]}';

purchase_reqObj = DeserializeJSON(purchase_req);
purchase_reqObj.key = key;

line_item = '{"id":"", "status":"", "key":"", "line_item":""}';
line_itemObj = DeserializeJSON(line_item);


for(i = 1; i LTE ArrayLen(returnlineItemsObj); i++){
	line_itemObj = DeserializeJSON(line_item);
	line_itemObj.key = returnlineItemsObj[i].id;
	line_itemObj.id = 'itemRow_' & i;
	StructDelete(returnlineItemsObj[i],"key");
	StructDelete(returnlineItemsObj[i],"id");
	line_itemObj.line_item = returnlineItemsObj[i];
	//arrayAppend(line_itemObj.line_item, returnlineItemsObj[i], true);
	arrayAppend(purchase_reqObj.line_items, line_itemObj, true);
 }

//arrayAppend(purchase_reqObj.order, order, true);
purchase_reqObj.order = order;
json = replace(serializer.serialize(purchase_reqObj),"\n","\\n","all");

//liArray = purchase_reqObj.line_items[1];
//li = liArray.line_item[1];

 

	
//liArray = purchase_reqObj.line_items[1];
//li = liArray.line_item[1];
//writeOutput(li.lineItem);

//test = DeserializeJSON(serializer.serialize(purchase_reqObj));
//liArray = test.line_items[1];
//li = liArray.line_item[1];
//writeOutput(li.lineItem);


//dockdate:1 purchasequantity:1 needdate:1 price:1 quantity:1 umo:1 estcost:1 lineitem:1 manufacturedesc:1

//#key#:#data[key]#
</cfscript>

<!---purchase_reqObj.order[1]['account_number']#<br><br><br>--->
#purchase_reqObj.order['account_number']#<br><br><br>

<!---
<cfloop from="1" to="#arrayLen(purchase_reqObj.line_items)#" index="j">
#purchase_reqObj.line_items[j]['id']#<br>
    <cfloop from="1" to="#arrayLen(purchase_reqObj.line_items[j].line_item)#" index="i">
        <cfset data = purchase_reqObj.line_items[j].line_item[i]>
        #data['lineItem']#
        #data['quantity']#
        #data['umo']#
        #data['estcost']#
        #data['needdate']#
        #data['manufacturedesc']#
        #data['purchasequantity']#
        #data['price']#
        #data['dockdate']#<br>
    </cfloop>
</cfloop>
--->

<cfloop from="1" to="#arrayLen(purchase_reqObj.line_items)#" index="j">
		#purchase_reqObj.line_items[j]['id']#<br>
        <cfset data = purchase_reqObj.line_items[j].line_item>
        #data['lineItem']#
        #data['quantity']#
        #data['umo']#
        #data['estcost']#
        #data['needdate']#
        #data['manufacturedesc']#
        #data['purchasequantity']#
        #data['price']#
        #data['dockdate']#<br>

</cfloop>

<cfloop from="1" to="#arrayLen(purchase_reqObj.line_items)#" index="i">
  <cfset data = purchase_reqObj.line_items[i].line_item>
  <cfloop collection="#data#" item="key">
    #key#:#data[key]#
  </cfloop> 
</cfloop>

 <!---cfdump var="#DeserializeJSON(returnSelect, 'true')#">--->
<cfdump var="#purchaseOrder#">
<cfdump var="#purchase_req_obj#">
</cfoutput>