<cfsilent>
	<cfapplication 
		name="#Hash(getCurrentTemplatePath())#" 
		clientmanagement="no" 
		sessionmanagement="yes" 
		loginstorage="session" 
		clientstorage="cookie"
		applicationtimeout="#CreateTimeSpan(0,1,0,0)#" 
		sessiontimeout="#CreateTimeSpan(0,1,0,0)#">
        
        <!--- Set up the application. --->
	<cfscript>
		THIS.Name = Hash(getCurrentTemplatePath());
		THIS.ApplicationTimeout = createTimeSpan(0,1,0,0);
		THIS.SessionTimeout = createTimeSpan(0,0,25,0);
		THIS.SessionManagement = true;
		THIS.SetClientCookies = true;
		THIS.ClientManagement = true;
		THIS.ClientStorage = "cookie";
	</cfscript>
	
	<cfscript>
		APPLICATION.rootDir = getDirectoryFromPath(getCurrentTemplatePath());
		APPLICATION.rootURL = "http://#CGI.HTTP_HOST#";
		APPLICATION.dsn = 'HME';
		APPLICATION.Datasource = "powsql";
		
		LOGIN = StructNew();
		LOGIN.maxAttempts = 5; 
		LOGIN.sessTimeout = CreateTimeSpan(0,1,0,0);
		
		ERR = StructNew();
		ERR.ErrorFound = false;
		ERR.ErrorMessage = "";
		
		SITE = StructNew();
		SITE.AdminEmail = "support@hme.com";
		SITE.ITRequest = "ITR@hme.com";
		SITE.MaintUser_IP = "192.168.106.39,10.40.3.48,192.168.105.196,192.168.106.232,192.168.106.36";
		//SITE.MaintUser_IP = "";
		SITE.DashUser_IP = "192.168.106.39,10.40.3.48,192.168.105.196";
		//SITE.DashUser_IP = "";
		SITE.IsMaintenance = true; //for putting site into maint mode. Currently turns off cferror.cfm
	</cfscript>

	<cfif StructKeyExists(URL, "reset")>
		<cfif StructClear(SESSION)></cfif>
	</cfif>
	
	<!---<cfif StructKeyExists(URL,"isdis") AND Find("WQEQNOCQRZX8OBG9OSDF",URL.isdis) NEQ 0><!---Bypass Login Req--->
		<cflock scope="session" type="exclusive" timeout="#LOGIN.sessTimeout#">
			<cfscript>
				SESSION.loginAttempts = 0;
				SESSION.IsLoggedIn = true;
			</cfscript>
		</cflock>
	</cfif>
	
	<!---Logon procedure--->
	<cfif (NOT StructKeyExists(SESSION, "IsLoggedIn"))>
        <cflock scope="session" type="exclusive" timeout="#LOGIN.sessTimeout#">
            <cfscript>
                SESSION.IsLoggedIn = false;
                SESSION.loginAttempts = 0;
            </cfscript>
        </cflock>
	</cfif>--->
</cfsilent>

<!---
<cfif (SESSION.IsLoggedIn)>
	<cflock scope="session" type="exclusive" timeout="#LOGIN.sessTimeout#">
		<cfscript>
			SESSION.loginAttempts = 0;
			SESSION.IsLoggedIn = true;
		</cfscript>
	</cflock>
<cfelse>
	<cfif SESSION.loginAttempts LTE LOGIN.maxAttempts>
		<cfif ((NOT IsDefined("FORM.j_username") OR FORM.j_username IS "") OR (NOT IsDefined("FORM.j_password") OR FORM.j_password IS ""))>
			<cfset ERR.ErrorFound = true>
			<cfset ERR.ErrorMessage = "Please provide your login information.">
			<cfinclude template="./login.cfm">
			<cfabort>
		<cfelse>
			<cfinclude template="./_tmp_loginCheck.cfm">
			
			<cfif ERR.ErrorFound>
				<cfinclude template="./login.cfm">
				<cfabort>
			<cfelse><!---Sucessful login--->
				<cflock scope="session" type="exclusive" timeout="#LOGIN.sessTimeout#">
					<cfscript>
						SESSION.loginAttempts = 0;
						SESSION.IsLoggedIn = true;
					</cfscript>
				</cflock>
			</cfif>
		</cfif>
	<cfelse>
		<cfset ERR.ErrorMessage = "Login attempts exceeded. Please wait a few minutes before trying again.">
		<cfset ERR.loginmax = true>
		<cfinclude template="./login.cfm">
		<cfabort>
	</cfif>
</cfif>
--->