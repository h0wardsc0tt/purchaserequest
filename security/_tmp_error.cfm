<cfsilent>
	<cfscript>
		SITE = StructNew();
		SITE.AdminEmail = "Web_Support@hme.com";
		SITE.ITRequest = "ITR@hme.com";
		SITE.MaintUser_IP = "192.168.106.232,127.0.0.1,192.168.1.4,12.46.199.2";
		SITE.IsMaintenance = false; //for putting site into maint mode
		SITE.IsDebug = false; //for putting the site into debug mode
	</cfscript>
	<cfif StructKeyExists(ERR, "Error") AND NOT SITE.IsDebug>
	<cfmail
		to="#SITE.AdminEmail#"
		from="no-reply@hme.com"
		subject="Web Site Error"
		type="html">
		<p>An error occurred at #DateFormat( Now(), "mmm d, yyyy" )# at #TimeFormat( Now(), "hh:mm TT" )#</p>
		
		<h3>Error</h3>
		
		<cfdump var="#ERR#"	label="Error object." />
		
		<h3>CGI</h3>
		
		<cfdump var="#CGI#" label="CGI object" />
		
		<h3>REQUEST</h3>
		
		<cfdump var="#REQUEST#" label="REQUEST object" top="5" />
		
		<h3>FORM</h3>
		
		<cfdump var="#FORM#" label="FORM object" top="5" />
		
		<h3>URL</h3> 
		
		<cfdump var="#URL#" label="URL object" top="5" />
		
		<h3>SESSION</h3>
	</cfmail>
	</cfif>
</cfsilent>

<cfset VARIABLES.Page_MetaTitle = "Error Occured">
<div id="Content">
	<div class="error_message">
		<h2 class="error">Internal Server Error</h2>
		<p>An internal error occured while processing your request.</p>
		<p>The system administrator has been notifed and is working to resolve this issue as quickly as possible.</p>
		<p>Should you continue to experience difficulty loading this page, please contact support directly</p>
	</div>
	<cfif ListFind(SITE.MaintUser_IP, CGI.REMOTE_ADDR)>
		<cfdump var="#ERR#"	label="Error object." />
	</cfif>
</div>