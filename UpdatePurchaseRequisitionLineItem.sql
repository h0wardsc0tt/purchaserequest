USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[UpdatePurchaseRequisitionLineItem]    Script Date: 9/13/2016 8:22:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[UpdatePurchaseRequisitionLineItem](
     @id INT,
     @key INT,
     @lineItem NVARCHAR(25) = NULL,
     @quantityRequested INT = NULL,
     @umo NVARCHAR(25) = NULL,
     @estimatedCost MONEY = NULL,
     @requestedDate NVARCHAR(25) = NULL,
     @manufacturePinDescription NVARCHAR(150) = NULL,
     @purchasedQuantity INT = NULL,
     @purchasedCost MONEY = NULL,
     @dockDate NVARCHAR(25) = NULL,
     @type NVARCHAR(25) = NULL,
	 @updateType NVARCHAR(10) OUTPUT,
	 @identity INT OUTPUT

) AS

SET  @identity = 0

if  @type IS NOT NULL and @type = 'delete'
	BEGIN
		SET @updateType = 'delete'
		DELETE FROM dbo.PurchaseRequisitionLineItems WHERE id =  @id
	END
ELSE
	BEGIN
		if EXISTS (SELECT id from dbo.PurchaseRequisitionLineItems WHERE id =  @id)
			BEGIN
				SET @updateType = 'update'
				UPDATE dbo.PurchaseRequisitionLineItems
				SET		lineItem = @lineItem,
						quantityRequested = ISNULL(@quantityRequested, quantityRequested), 
						umo = ISNULL(@umo, umo),
						estimatedCost = ISNULL(@estimatedCost, estimatedCost),
						requestedDate = ISNULL(@requestedDate, requestedDate),
						manufacturePinDescription = ISNULL(@manufacturePinDescription, manufacturePinDescription),
						purchasedQuantity = ISNULL(@purchasedQuantity, purchasedQuantity),
						purchasedCost = ISNULL(@purchasedCost, purchasedCost),
						dockDate = ISNULL(@dockDate, dockDate),
						modifiedDate = GETDATE()
				WHERE id =  @id
			END
		ELSE
			BEGIN
				SET @updateType = 'insert'
				INSERT INTO dbo.PurchaseRequisitionLineItems
					([key], lineItem, quantityRequested ,umo, estimatedCost, requestedDate, manufacturePinDescription, purchasedQuantity, purchasedCost, dockDate)
				VALUES
					(@key, @lineItem, @quantityRequested ,@umo, @estimatedCost, @requestedDate, @manufacturePinDescription, @purchasedQuantity, @purchasedCost, @dockDate
)
				SET  @identity = @@IDENTITY
			END
	END

